# Eeyek

Eeyek is a font for the Meetei Mayek (Meitei Mayek) script.

Eeyek is Copyright (c) 1999-2019 Pravabati Chingangbam and Tabish (tabishq@gmail.com) and is released under under the [SIL Open Font License (scripts.sil.org/OFL)](http://scripts.sil.org/OFL).

For details - including any Reserved Font Names - see [OFL.txt](OFL.txt).

For practical information about using, modifying and redistributing this font see [OFL-FAQ.txt](OFL-FAQ.txt).

For more details about this project, including changelog and acknowledgements see [FONTLOG.txt](FONTLOG.txt).

Special permission has been granted to SIL to use the Reserved Font Name "Eeyek" for this project.
